#!/bin/bash
#
# listado https://reisub.nsupdate.info/wiki/?page=SMail/ListadeGruposyCanales
# Creado por SMail:guaguaa@smail.nsupdate.info/
read -p "Grupos (g) Canales (c)?" CONT
if [ "$CONT" = "g" ]; then
#grupos
echo "1) smail_group@reisub.nsupdate.info/smail/"

echo "2) reisub_group@reisub.nsupdate.info/smail/"

echo "3) gnulinux@reisub.nsupdate.info/smail/"

echo "4) testchan@reisub.nsupdate.info/smail/"

echo "5) debian_group@smail.nsupdate.info/"

read -p "Cual leer (num)?" choice
case "$choice" in 
  1|1 ) echo "smail_group@reisub.nsupdate.info/smail/"
curl https://reisub.nsupdate.info/smail/mailbox/smail_group/mails/index.php
echo "ejecute curl  https://reisub.nsupdate.info/smail_group/mailbox/ID";;
  2|2 ) echo "reisub_group@reisub.nsupdate.info/smail/"
curl https://reisub.nsupdate.info/smail/mailbox/reisub_group/mails/index.php
echo "ejecute curl  https://reisub.nsupdate.info/reisub_group/mailbox/ID";;
  3|3 ) echo "gnulinux@reisub.nsupdate.info/smail/"
curl https://reisub.nsupdate.info/smail/mailbox/gnulinux/mails/index.php
echo "ejecute curl  https://reisub.nsupdate.info/gnulinux/mailbox/ID";;
  4|4 ) echo "testchan@reisub.nsupdate.info/smail/"
curl https://reisub.nsupdate.info/smail/mailbox/testchan/mails/index.php
echo "ejecute curl  https://reisub.nsupdate.info/testchan/mailbox/ID";;
  5|5 ) echo "debian_group@smail.nsupdate.info/"
curl https://smail.nsupdate.info/mailbox/debian_group/mails/index.php
echo "ejecute curl  https://reisub.nsupdate.info/debian_group/mailbox/ID";;
  * ) echo "invalid";;
esac
else
#canales
echo "1) abtest@reisub.nsupdate.info/smail/"

echo "2) reisub@reisub.nsupdate.info/smail/"

echo "3) smail@reisub.nsupdate.info/smail/"

read -p "Cual leer (num)?" choice
case "$choice" in 
  1|1 ) echo "abtest@reisub.nsupdate.info/smail/"
curl https://reisub.nsupdate.info/smail/mailbox/abtest/mails/1.php
read -rsp $'Presionar una tecla para siguiente...\n'
curl https://reisub.nsupdate.info/smail/mailbox/abtest/mails/2.php
read -rsp $'Presionar una tecla para siguiente...\n'
curl https://reisub.nsupdate.info/smail/mailbox/abtest/mails/3.php
read -rsp $'Presionar una tecla para siguiente...\n'
curl https://reisub.nsupdate.info/smail/mailbox/abtest/mails/4.php;;
  2|2 ) echo "reisub@reisub.nsupdate.info/smail/"
curl https://reisub.nsupdate.info/smail/mailbox/reisub/mails/1.php
read -rsp $'Presionar una tecla para siguiente...\n'
curl https://reisub.nsupdate.info/smail/mailbox/reisub/mails/2.php
read -rsp $'Presionar una tecla para siguiente...\n'
curl https://reisub.nsupdate.info/smail/mailbox/reisub/mails/3.php;;
  3|3 ) echo "smail@reisub.nsupdate.info/smail/"
curl https://reisub.nsupdate.info/smail/mailbox/smail/mails/1.php
read -rsp $'Presionar una tecla para siguiente...\n'
curl https://reisub.nsupdate.info/smail/mailbox/smail/mails/2.php
read -rsp $'Presionar una tecla para siguiente...\n'
curl https://reisub.nsupdate.info/smail/mailbox/smail/mails/3.php;;
  * ) echo "invalid";;
esac
fi